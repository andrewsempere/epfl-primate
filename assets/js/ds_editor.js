///////////////////////////////////////////////////////////
// DS_EDITOR
// 3JS editor for Macacque  
//
// Author: Andrew Sempere
// DigitalScenographic.com
// 10/19/2016
///////////////////////////////////////////////////////////

var allow_editing = true;
var backgroundColor_editingMode = 0x666666;
var ambientOn=false;

// Reset the scene
function resetScene(){
	
	// If we don't allow editing, just leave
	if(!allow_editing || !editingMode){ return; }
	
	controls.reset();

	camera.position.x=0;
	camera.position.y=0;
	camera.position.z=800;
	lookAt.x=0;
	lookAt.y=0;
	lookAt.z=0;
	
	
	if (typeof modelObject !== 'undefined') {
		modelObject.position.x=0;
		modelObject.position.y=0;
		modelObject.position.z=0;
		modelObject.rotation.x=0;
		modelObject.rotation.y=0;
		modelObject.rotation.z=0;
		
	}
}

// Toggle editing mode
var axis, axis2;
function modeToggle() {
	
	// If we don't allow editing, just leave
	if(!allow_editing){ return;}

	// Toggle editingmode on/off
	if(!editingMode){
		resetScene();
		axis = new THREE.AxisHelper( 5000 );
		scene.add(axis);
		axis2 = new THREE.AxisHelper( 100 );
		//modelObject.add(axis2);
		controls.enabled = true;
		renderer.setClearColor(backgroundColor_editingMode);
		 //$("#content").hide();
		
		
	}else{
		scene.remove(axis);
		renderer.setClearColor(backgroundColor);
		controls.enabled = false;
		 //$("#content").show();
		
	}
	editingMode = !editingMode;

}	





// Keys we handle
// List of codes: https://css-tricks.com/snippets/javascript/javascript-keycodes/
var map = {	 65:false  // a
			,69:false  // e
		   	,80:false  // p 
		   	,82:false  // r
			,76:false  // l
			
			,88:false  // x
			,89:false  // y
			,90:false  // z
	
		   	,37:false  // left
		   	,38:false  // up
		   	,39:false  // right
		   	,40:false  // down 

		   };

// Keys we suppress the default action on
var suppress = new Array(33,34,35,36,37,38,39,40);
		   
$(document).keydown(function(e) {

	// Exit if we're not in e)ditingMode
	if(!allow_editing || (!editingMode && e.keyCode != 69)){ return; }

	// Prevent arrow keys / pgup etc from scrolling browser
    if($.inArray(e.keyCode,suppress) > -1) {
       e.preventDefault();
    }

	// Finally the key handlers
    if (e.keyCode in map) {
        map[e.keyCode] = true;


		// Step amount, cut in half if alt held down 
		var stepAmount = 5;
		var rotationAmount = radiansFromDegrees(5);
		if(e.altKey){
			stepAmount *= .25;
			rotationAmount *= .25;
		}
			
		// x 
    	if(map[88]){
    		// left
    		if(map[37]){
				modelObject.position.x += stepAmount;

    		// right
    		}else if(map[39]){
				modelObject.position.x -= stepAmount;
				
    		// up
    		}else if(map[38]){
				modelObject.add(axis2);
				modelObject.rotation.x -= rotationAmount;
				
    		// down
    		}else if(map[40]){
				modelObject.add(axis2);
				modelObject.rotation.x += rotationAmount;
    		}
    	
		// y
    	}else if(map[89]){
    		// left
    		if(map[37]){
				modelObject.position.y += stepAmount;

    		// right
    		}else if(map[39]){
				modelObject.position.y -= stepAmount;
				
    		// up
    		}else if(map[38]){
				modelObject.add(axis2);
				modelObject.rotation.y -= rotationAmount;
				
    		// down
    		}else if(map[40]){
				modelObject.add(axis2);
				modelObject.rotation.y += rotationAmount;
    		}

    	// z
    	}else if(map[90]){
    		// left
    		if(map[37]){
				modelObject.position.z += stepAmount;

    		// right
    		}else if(map[39]){
				modelObject.position.z -= stepAmount;
				
    		// up
    		}else if(map[38]){
				modelObject.add(axis2);
				modelObject.rotation.z -= rotationAmount;
				
    		// down
    		}else if(map[40]){
				modelObject.add(axis2);
				modelObject.rotation.z += rotationAmount;
    		}
    	
    	// l
    	}else if(map[76]){
			
			// a)mbient
			if (map[65]) {
				ambientOn=!ambientOn;
				if(ambientOn){
					scene.add(light_ambient);	
				}else{
					scene.remove(light_ambient);	
				}
				
    		// r)eset
    		}else if(map[82]){
				light_directional.position.set(0,0,1);
			
    		// left
    		}else if(map[37]){
				light_directional.position.x += (rotationAmount*.25);

    		// right
    		}else if(map[39]){
				light_directional.position.x -= (rotationAmount*.25);
				
    		// up
    		}else if(map[38]){
				light_directional.position.y += (rotationAmount*.25);
				
    		// down
    		}else if(map[40]){
				light_directional.position.y -= (rotationAmount*.25);
    		}
    
    	// e)dit toggle
    	}else if (map[69]) {
        	modeToggle();

    	// p)rint to console
    	}else if (map[80]) {
        	printSettings();
        	        	
    	// r)eset
    	}else if (map[82]) {
        	resetScene();
        }
		
    }
}).keyup(function(e) {
    if (e.keyCode in map) {
        map[e.keyCode] = false;
    }
	
	modelObject.remove(axis2);
});


// deg/rad conversion
function degreesFromRadians(radians){
	return radians * (180/Math.PI);
}
function radiansFromDegrees(degrees){
	return degrees * (Math.PI/180);
}

// Print settings out
function printSettings(){
	console.log( "var paragraphPosition=new Array(");
	$( "p" ).each(function( index ) {
		var offset=118;
		var bottomOffset=39;
		var topOfParagraph = $(this).offset().top-offset;
		var bottomOfParagraph = (topOfParagraph+$(this).height())+bottomOffset;
		index++;
		console.log( "{begin:"+topOfParagraph + ", end:" +bottomOfParagraph+ "},\n" );
	});
	console.log( ");");
	
	// Ligting:
	console.log("\n");
	console.log("LIGHTING: Ambient "+(ambientOn?"ON":"OFF")+" ("+light_directional.position.x+","+light_directional.position.y+","+light_directional.position.z+")");
	
	
	// Model settings
	console.log("\n");
	
	
	//console.log("CAM: " +camera.position.x+","+camera.position.y+","+camera.position.z);
	//console.log("LOOKAT: " +lookAt.x+","+lookAt.y+","+lookAt.z);
	//console.log("OBJECT: " +modelObject.position.x+","+modelObject.position.y+","+modelObject.position.z);
	
	var scrollPos = $("#content").scrollTop();
	console.log("{scrollPosition: *" +
				",\tcameraX:"+camera.position.x+ 
				",\tcameraY:"+camera.position.y+
				",\tcameraZ:"+camera.position.z+ 		
				",\tobjectX:"+modelObject.position.x+
				",\tobjectY:"+modelObject.position.y+
				",\tobjectZ:"+modelObject.position.z+
				",\tobjectRotX:"+modelObject.rotation.x+
				",\tobjectRotY:"+modelObject.rotation.y+
				",\tobjectRotZ:"+modelObject.rotation.z+"}");
}
