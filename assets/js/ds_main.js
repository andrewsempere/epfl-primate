///////////////////////////////////////////////////////////
// DS_MAIN
// 3JS config for Macacque  
//
// Author: Andrew Sempere
// DigitalScenographic.com
// 10/19/2016
///////////////////////////////////////////////////////////


// Edit these
var model = 'Singe_All_Elementsv6_hole';
var backgroundColor = 0x080808;
var ambientLightColor = 0xAAAAAA;
var directionalLightColor = 0x33333;
var transparent = false;

// Do not edit below /////////////////////////////////////
var modelObject;
var light_ambient;
var light_directional;
var threeDeeAssetDir = "assets/models/";
var objFile = model + ".obj";
var mtlFile = model + ".mtl";
var editingMode = false;
var scaleFactor = 20;

var container;
var camera, scene, renderer;
var mouseX = 0, mouseY = 0;
var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;

var controls;

// Loading modal 
$("body").addClass("loading");

// Go!
init();
animate();

// Setup
function init() {
	
	container = document.createElement('div');
	//container.style.cssText = 'position:relative;top:5em';
	
	document.body.appendChild(container);
	
	// Camera
	camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 1, 4500 );
	scene = new THREE.Scene();
	
	// Lighting
	light_ambient = new THREE.AmbientLight(ambientLightColor);
	light_directional= new THREE.DirectionalLight(directionalLightColor);
	light_directional.position.set(-10.3125,13.125,1);
	lookAt = scene.position;
	scene.add(light_directional);
	scene.add(light_ambient);	
	
	// Load manager
	var manager = new THREE.LoadingManager();
	manager.onProgress = function ( item, loaded, total ) {
		console.log( item, loaded, total );
	};
	var texture = new THREE.Texture();
	var onProgress = function ( xhr ) {
		if ( xhr.lengthComputable ) {
			var percentComplete = xhr.loaded / xhr.total * 100;
			console.log(xhr.currentTarget.responseURL + ": "+ Math.round(percentComplete, 2) + '% downloaded' );
		}
	};
	var onError = function ( xhr ) { 
		$("body").removeClass("loading");
		$("body").addClass("error");	
	};

	// MTL/OBJ Loader
	var mtlLoader = new THREE.MTLLoader(manager);
	mtlLoader.setPath(threeDeeAssetDir);
	mtlLoader.load(mtlFile, 
	
		function(materials) {	
			materials.preload();		
			var objLoader = new THREE.OBJLoader(manager);

			objLoader.setMaterials(materials);
			objLoader.setPath(threeDeeAssetDir);
			objLoader.load(objFile, function ( object ) {

			// Traverse the object and makes sure the textures are doublesided
			object.traverse( function ( child ) {
	        if ( child instanceof THREE.Mesh ) {
	            child.material.side = THREE.DoubleSide;
				if(transparent){
					child.material.transparent = true;
					child.material.opacity=0.8;
				}
	        }
		});

		// start with the object offscreen
		object.position.z=-9000;
		object.scale.x=scaleFactor;
		object.scale.y=scaleFactor;
		object.scale.z=scaleFactor;
		scene.add( object );	
		modelObject=object;		

	},onProgress, onError);
	});

	// Renderer
	renderer = new THREE.WebGLRenderer({ antialias: true });
	renderer.setPixelRatio( window.devicePixelRatio );
	renderer.setClearColor(backgroundColor);
	onWindowResize();
	container.appendChild( renderer.domElement );
	document.addEventListener( 'mousemove', onDocumentMouseMove, false );
	window.addEventListener( 'resize', onWindowResize, false );
	
	// Controls
	controls = new THREE.TrackballControls( camera );
	controls.rotateSpeed = 1.0;
	controls.zoomSpeed = 1.2;
	controls.panSpeed = 0.8;
	controls.noZoom = false;
	controls.noPan = false;
	controls.staticMoving = true;
	controls.dynamicDampingFactor = 0.3;
	controls.enabled = false;
	
}

function animate() {
	requestAnimationFrame(animate);
	render();
}

// Every frame
function render() {
	camera.up = new THREE.Vector3(0,1,0);
	camera.lookAt(lookAt);
	controls.update();
	//camera.position.y += ( - mouseY - camera.position.y ) * .25;
	renderer.render( scene, camera );
}

function onWindowResize() {
	var minSize = 1024;
	width = window.innerWidth<minSize?minSize:window.innerWidth;
	height = window.innerHeight<minSize?minSize:window.innerHeight;
	windowHalfX = width / 2;
	windowHalfY = height / 2;
	camera.aspect = width / height;
	camera.updateProjectionMatrix();
	renderer.setSize(width,height);
}

function onDocumentMouseMove( event ) {
	mouseX = ( event.clientX - windowHalfX ) / 2;
	mouseY = ( event.clientY - windowHalfY ) / 2;
}

